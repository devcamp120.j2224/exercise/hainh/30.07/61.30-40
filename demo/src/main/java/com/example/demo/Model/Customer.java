package com.example.demo.Model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "customer")
public class Customer {

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private int   id;

@Column(name = "full_name")
private String	fullname;

@Column(name = "email")
private String	email;

@Column(name = "phone")
private String	phone;

@Column(name = "dia_chi")
private String	Address;

@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
@JsonManagedReference
private List<Order> order;


public Customer() {
}
public Customer(int id, String fullname, String email, String phone, String address, List<Order> order) {
    this.id = id;
    this.fullname = fullname;
    this.email = email;
    this.phone = phone;
    Address = address;
    this.order = order;
}
public long getId() {
    return id;
}
public void setId(int id) {
    this.id = id;
}
public String getFullname() {
    return fullname;
}
public void setFullname(String fullname) {
    this.fullname = fullname;
}
public String getEmail() {
    return email;
}
public void setEmail(String email) {
    this.email = email;
}
public String getPhone() {
    return phone;
}
public void setPhone(String phone) {
    this.phone = phone;
}
public String getAddress() {
    return Address;
}
public void setAddress(String address) {
    Address = address;
}
public List<Order> getOrders() {
    return order;
}
public void setOrders(List<Order> orders) {
    this.order = orders;
}
}
