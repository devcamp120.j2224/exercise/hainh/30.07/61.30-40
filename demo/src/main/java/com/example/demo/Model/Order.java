package com.example.demo.Model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "order123")
public class Order {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long orderId ;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    @JsonIgnore
    private Customer customer;


    @Column(name = "order_code")
    private String orderCode ;

    @Column(name= "pizza_size")
    private String pizzaSize ; 

    @Column(name= "pizza_type")
    private String pizzaType ; 

    @Column(name= "voucher_code")
    private String voucherCode ; 

    @Column(name= "price")
    private long price ; 

    @Column(name= "paid")
    private long paid ;
    
    @OneToMany(mappedBy = "order" , cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Product> product ;

  

    public long getId() {
        return orderId;
    }

    public void setId(long id) {
        this.orderId = id;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(String pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public String getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPaid() {
        return paid;
    }

    public void setPaid(long paid) {
        this.paid = paid;
    }

    public Set<Product> getProducts() {
        return product;
    }

    public void setProducts(Set<Product> product) {
        this.product = product;
    }

    public Order() {
    }

    public Order(long id, String orderCode, String pizzaSize, String pizzaType, String voucherCode, long price,
            long paid, Set<Product> product) {
        this.orderId = id;
        this.orderCode = orderCode;
        this.pizzaSize = pizzaSize;
        this.pizzaType = pizzaType;
        this.voucherCode = voucherCode;
        this.price = price;
        this.paid = paid;
        this.product = product;
    }

   
    
}
