package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Customer;

public interface iRespositoryCustomer extends JpaRepository <Customer , Long>{
    Customer findById(int id);
}
