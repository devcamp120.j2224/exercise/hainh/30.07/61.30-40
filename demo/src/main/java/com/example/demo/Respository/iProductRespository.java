package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Product;

public interface iProductRespository extends JpaRepository <Product , Long>{

}
