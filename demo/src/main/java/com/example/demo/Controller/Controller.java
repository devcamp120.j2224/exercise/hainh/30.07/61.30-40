package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Customer;
import com.example.demo.Model.Order;
import com.example.demo.Model.Product;
import com.example.demo.Respository.iOrderRespository;
import com.example.demo.Respository.iRespositoryCustomer;

@RestController
@CrossOrigin
public class Controller {
    
    @Autowired
    iRespositoryCustomer iCusRes ;

    @GetMapping("order")
    public ResponseEntity<List<Order>> getOrdersListByCustomerId(@RequestParam(name = "customerId") int customerId) {
        try {
            Customer customer =  iCusRes.findById(customerId);
            if(customer!=null){
                return new ResponseEntity<>(customer.getOrders(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> getCustomersList(){
        try {
            List<Customer> customerList = new ArrayList<>();
            iCusRes.findAll().forEach(customerList::add);
            return new ResponseEntity<>(customerList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @Autowired
    iOrderRespository iOrderRespository;

    @GetMapping("product")
    public ResponseEntity<Set<Product>> getProductList(@RequestParam (value = "orderId") long order){
        try{
            Order vOrder = iOrderRespository.findByOrderId(order);
            if(vOrder != null){
                return new ResponseEntity<>(vOrder.getProducts(), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
